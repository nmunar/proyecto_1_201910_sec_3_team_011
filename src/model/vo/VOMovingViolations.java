package model.vo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Representation of a Trip object
 */
public class VOMovingViolations implements Comparable<VOMovingViolations>{


	private int objectId;

	private String location;

	private LocalDateTime ticketIssueDate;

	private double totalPaid;

	private String accidentIndicator;

	private String violationDescription;

	private double fineAmt;

	private String violationCode;

	private String date;

	private String adressId = "";

	private String streetSegId = "";

	private double PENALTY1 = 0.0;

	private double PENALTY2 = 0.0;

	public  VOMovingViolations(String pObjectId, String pLocation, String pTicketIssueDate, String pTotalPaid, String pAccidentIndicator, String pViolationDescription, String pfineAmt, String pViolationCode, String pAdressId, String pStreetSegId,String pPenalty1,String pPenalty2) {

		objectId = Integer.parseInt(pObjectId);
		location = pLocation;
		date = pTicketIssueDate;
		if(pTicketIssueDate.contains("-")) {

			ticketIssueDate = convertirFecha_Hora_LDT(date);
		} else {
			ticketIssueDate = convertirFecha_Hora_LDT("2018-01-01T06:30:00.000Z");
		}
		totalPaid = Double.parseDouble(pTotalPaid);
		accidentIndicator = pAccidentIndicator;
		violationDescription = pViolationDescription;
		fineAmt= Double.parseDouble(pfineAmt);
		violationCode = pViolationCode;
		adressId = pAdressId;
		streetSegId = pStreetSegId;
		if(pPenalty1==null||pPenalty1.isEmpty())
		{
			PENALTY1 = 0.0;
		}else
		{
			PENALTY1 = Double.parseDouble(pPenalty1);
		}

		if(pPenalty2==null || pPenalty2.isEmpty())
		{
			PENALTY2=0.0;
		}else
		{
			PENALTY2 = Double.parseDouble(pPenalty2);
		}


	}

	public String getAdressId() {

		return adressId;

	}

	public String getStreetSegId() {

		return streetSegId;

	}

	public double getPenalty1()
	{
		return PENALTY1;
	}

	public double getPenalty2()
	{
		return PENALTY2;
	}


	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectId() {
		// TODO Auto-generated method stub
		return objectId;
	}	


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		// TODO Auto-generated method stub
		return location;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public LocalDateTime getTicketIssueDate() {

		return ticketIssueDate;

	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public double getTotalPaid() {
		// TODO Auto-generated method stub
		return totalPaid;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String  getAccidentIndicator() {
		// TODO Auto-generated method stub
		return accidentIndicator;
	}

	public int darHora()
	{
		return ticketIssueDate.getHour();
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		// TODO Auto-generated method stub
		return violationDescription;
	}

	public double getFineAmt()
	{
		return fineAmt;
	}

	public String getViolationCode() {

		return violationCode;

	}

	@Override
	public int compareTo(VOMovingViolations o) {
		// TODO Auto-generated method stub

		int respuesta = this.objectId() - o.objectId();

		if(respuesta > 0)  {

			return 1;

		} else if(respuesta < 0) {

			return -1;

		} else {

			return 0;

		}
	}
	public int compareFecha(VOMovingViolations o) {
		// TODO Auto-generated method stub
		int deter = 90;
		if(o.getTicketIssueDate().equals(this.getTicketIssueDate()))
		{
			if(o.objectId()==this.objectId)
			{
				deter = 0;
			}else if(o.objectId()<this.objectId)
			{
				deter = 1;
			}else if(o.objectId()>this.objectId)
			{
				deter = -1;
			}

		}else if(o.getTicketIssueDate().compareTo(this.getTicketIssueDate())<0)
		{
			deter = 1;
		}else if(o.getTicketIssueDate().compareTo(this.getTicketIssueDate())>0)
		{
			deter = -1;
		}
		return deter;
	}

	public int compareSteetId(VOMovingViolations o) {
		// TODO Auto-generated method stub
		int deter = 90;
		if(o.getStreetSegId().equals(getStreetSegId()))
		{
			if(o.getTicketIssueDate().compareTo(getTicketIssueDate()) == 0)
			{

				deter = 0;

			}else if(o.getTicketIssueDate().compareTo(getTicketIssueDate()) < 0)
			{
				deter = 1;

			} else if(o.getTicketIssueDate().compareTo(getTicketIssueDate()) > 0)
			{
				deter = -1;
			}

		}else if(o.getStreetSegId().compareTo(getStreetSegId())<0)
		{
			deter = 1;
		}else if(o.getStreetSegId().compareTo(getStreetSegId()) > 0)
		{
			deter = -1;
		}
		return deter;
	}

	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}


	public int compareViolationDesc(VOMovingViolations o) 
	{
		int resp =99;
		if(this.getViolationDescription().compareTo(o.getViolationDescription())==0)
		{
			resp= 0;
		}else
			if(this.getViolationDescription().compareTo(o.getViolationDescription()) > 0)  {

				resp= 1;

			} else if(this.getViolationDescription().compareTo(o.getViolationDescription()) < 0) {

				resp= -1;
			} 
		return resp;
	}

	public int compareFINEAMT(VOMovingViolations w) 
	{
		int resp =99;
		if(this.getFineAmt()== w.getFineAmt())
		{
			resp= 0;
		}else	
			if(this.getFineAmt()>w.getFineAmt())  {

				resp= 1;

			} else if(this.getFineAmt()<w.getFineAmt()) {

				resp= -1;
			} 
		return resp;
	}

	public int compareTOTALPAID(VOMovingViolations w) 
	{
		int resp =99;
		if(this.getTotalPaid()== w.getTotalPaid())
		{
			resp= 0;
		}else	
			if(this.getTotalPaid()>w.getTotalPaid())  {

				resp= 1;

			} else if(this.getTotalPaid()<w.getTotalPaid()) {

				resp= -1;
			} 
		return resp;
	}

	public int compareHour(VOMovingViolations w) 
	{
		int resp =99;
		if(this.darHora()== w.darHora())
		{
			resp= 0;
		}else	
			if(this.darHora()>w.darHora())  {

				resp= 1;

			} else if(this.darHora()<w.darHora()) {

				resp= -1;
			} 
		return resp;
	}

	public int compareViolationCode(VOMovingViolations w) 
	{
		int resp =99;
		if(this.getViolationCode()== w.getViolationCode())
		{
			resp= 0;
		}else	
			if(this.getViolationCode().compareTo(w.getViolationCode())>0 )  {

				resp= 1;

			} else if(this.getViolationCode().compareTo(w.getViolationCode())<0 ) {

				resp= -1;
			} 
		return resp;
	}
}
