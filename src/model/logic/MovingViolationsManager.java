package model.logic;

import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import com.opencsv.CSVReader;
import api.IMovingViolationsManager;
import jdk.net.NetworkPermission;
import model.vo.VOMovingViolations;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.util.Sort;

public class MovingViolationsManager implements IMovingViolationsManager {


	private Stack<VOMovingViolations> stackInfracciones = new Stack<VOMovingViolations>();

	private Queue<VOMovingViolations> queueInfracciones = new Queue<VOMovingViolations>();

	private ArrayList<Integer> listaObjectIdRepetidos = new ArrayList<Integer>();

	private String respuestaCuat = "";

	private VOMovingViolations[] listaObjectId;

	private double media=0.0;

	Scanner sc = new Scanner(System.in);

	String objectId = "";
	String location = "";
	String ticketIssueDate ="";
	String totalPaid ="";
	String accidentIndicator ="";
	String violationDescription ="";
	String sumaFINEAMT ="";
	String violationCode ="";
	String adressId = "";
	String streetSegId = "";
	String PENALTY1 =""; 
	String PENALTY2 ="";

	double primerMes=0;
	double segundoMes=0;
	double tercerMes=0;
	double cuartoMes=0;

	int cero = 0;
	int uno = 0;
	int dos = 0;
	int tres = 0;



	public Stack<VOMovingViolations> darStack() {

		return stackInfracciones;

	}

	public Queue<VOMovingViolations> darQueue() {

		return queueInfracciones;

	}

	int pos = 0;
	public void loadMovingViolations(int pRespuestaCuat, int pMetodo) {

		try {

			String[] archCSV = new String[4];
			String[] meses = new String[4];

			Integer[] cantidades = new Integer[4];

			if(pRespuestaCuat == 1) {

				archCSV[0] = "./data/Moving_Violations_Issued_in_January_2018.csv";
				archCSV[1] = "./data/Moving_Violations_Issued_in_February_2018.csv";
				archCSV[2] = "./data/Moving_Violations_Issued_in_March_2018.csv";
				archCSV[3] = "./data/Moving_Violations_Issued_in_April_2018.csv";

				meses[0] = "Enero";
				meses[1] = "Febrero";
				meses[2] = "Marzo";
				meses[3] = "Abril";

				listaObjectId = new VOMovingViolations[373543];



			} else if(pRespuestaCuat == 2) {

				archCSV[0] = "./data/Moving_Violations_Issued_in_May_2018.csv";
				archCSV[1] = "./data/Moving_Violations_Issued_in_June_2018.csv";
				archCSV[2] = "./data/Moving_Violations_Issued_in_July_2018.csv";
				archCSV[3] = "./data/Moving_Violations_Issued_in_August_2018.csv";

				meses[0] = "Mayo";
				meses[1] = "Junio";
				meses[2] = "Julio";
				meses[3] = "Agosto";

				listaObjectId = new VOMovingViolations[484951];


			} else if(pRespuestaCuat == 3) {

				archCSV[0] = "./data/Moving_Violations_Issued_in_September_2018.csv";
				archCSV[1] = "./data/Moving_Violations_Issued_in_October_2018.csv";
				archCSV[2] = "./data/Moving_Violations_Issued_in_November_2018.csv";
				archCSV[3] = "./data/Moving_Violations_Issued_in_December_2018.csv";

				meses[0] = "Septiembre";
				meses[1] = "Octubre";
				meses[2] = "Noviembre";
				meses[3] = "Diciembre";

				listaObjectId = new VOMovingViolations[405202];

			}

			int contTotal = 0;

			for(int i = 0; i < 4; i++) {

				int cont = 0;


				CSVReader csvReader = new CSVReader(new FileReader(archCSV[i]));
				String[] fila = null;
				csvReader.readNext();
				while((fila = csvReader.readNext())!= null ) {


					objectId = fila[0];
					location = fila[2];
					ticketIssueDate = fila[13];
					sumaFINEAMT = fila[8];
					totalPaid = fila[9];
					accidentIndicator = fila[12];
					violationDescription = fila[15];
					violationCode = fila[14];
					adressId = fila[3];
					streetSegId = fila[4];
					PENALTY1 = fila[10];
					PENALTY2 = fila[11];

					//Se crea el objeto VOMovingViolations

					VOMovingViolations infraccion = new VOMovingViolations(objectId, location, ticketIssueDate, totalPaid, accidentIndicator, violationDescription,sumaFINEAMT, violationCode,adressId,streetSegId,PENALTY1,PENALTY2);



					stackInfracciones.push(infraccion);
					queueInfracciones.enqueue(infraccion);

					//Se agrega al arreglo la infracci�n
					listaObjectId[pos] = infraccion;

					if (i ==0)
					{
						primerMes+= (infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					}else if(i ==1)
					{
						segundoMes+=(infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					}else if(i ==2)
					{
						tercerMes+=(infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					}else if(i ==3)
					{
						cuartoMes+=(infraccion.getFineAmt()+infraccion.getTotalPaid()+infraccion.getPenalty1()+infraccion.getPenalty2());
					}

					cont++;
					pos++;
				}
				contTotal = contTotal + cont;
				cantidades[i] = cont;

				csvReader.close();

			}


			System.out.println("Cantidad de infracciones por mes:");

			for(int j = 0; j < 4; j++) {

				System.out.println(meses[j] + " = " + cantidades[j]);

			}
			System.out.println("Cantidad de infracciones totales por el cuatrimestre elegido: " + contTotal);

		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Verificar si todos los object id de un cuatrimestre son �nicos.
	 * Mostrar por consola si no hay ninguno repetido. Si lo hay, mostrar los cuales se repitieron.
	 */
	@Override
	public void verifyObjectIDIsUnique() {

		VOMovingViolations[] aux = new VOMovingViolations[listaObjectId.length]; 

		if(listaObjectId.length != 0) {

			Sort.ordenarMergeSort(listaObjectId, aux, 0, listaObjectId.length-1,1);


			for(int i = 1; i < listaObjectId.length-1; i++) {

				if(!(listaObjectId[i].objectId() > listaObjectId[i-1].objectId())) {

					listaObjectIdRepetidos.add(listaObjectId[i].objectId());

				}

			}

			if(listaObjectIdRepetidos.size() != 0) {

				for(int i = 0; i < listaObjectIdRepetidos.size(); i++) {

					System.out.println(listaObjectIdRepetidos.get(i));

				}

			} else {

				System.out.println("No existen Object Ids repetidos.");

			}

		} else {

			System.out.println("No se cargaron los datos.");

		}

	}

	/**
	 * Consultar	infracciones	por	fecha/hora	inicial	y	fecha/hora	final. 
	 * Para	las	infracciones resultantes	 mostrar	OBJECTID	 y	 TICKETISSUEDATE.
	 * @param pFechaInicial fecha inicial del rango.
	 * @param pFechaFinal fecha final del rango.
	 * @return devuelve cola que contiene aquellas infracicones dentro del rango de fecha.
	 */
	@Override
	public Queue<VOMovingViolations> darRangoInfracciones(LocalDateTime pFechaInicial, LocalDateTime pFechaFinal) {


		VOMovingViolations[] aux = new VOMovingViolations[listaObjectId.length];
		ArrayList<VOMovingViolations> arreglo = new ArrayList<VOMovingViolations>();

		Queue<VOMovingViolations> cola = new Queue<VOMovingViolations>();

		Sort.ordenarMergeSort(listaObjectId, aux, 0, listaObjectId.length-1,2);

		for(int i = 0; i < listaObjectId.length; i++ ) {

			if( (listaObjectId[i].getTicketIssueDate().compareTo(pFechaInicial)  == 0 || listaObjectId[i].getTicketIssueDate().compareTo(pFechaInicial)  > 0)  &&  (listaObjectId[i].getTicketIssueDate().compareTo(pFechaFinal)  == 0 || listaObjectId[i].getTicketIssueDate().compareTo(pFechaFinal)  < 0) ) {

				cola.enqueue(listaObjectId[i]);
				arreglo.add(listaObjectId[i]);

			}
		}

		Scanner sc = new Scanner(System.in);

		System.out.println("�Desea ver la cantidad de infracciones resultantes?");
		System.out.println("Digite 1 para decir s�. Digite cualquier otro n�mero de lo contrario.");

		int respuesta = sc.nextInt();

		if(respuesta == 1) {

			System.out.println("�Cu�ntos datos quiere visualizar?");
			System.out.println("Digite un n�mero del 0-"+(arreglo.size()));

			int respuesta2 = sc.nextInt();

			for (int i = 0; i < respuesta2; i++) {

				System.out.println(arreglo.get(i).getTicketIssueDate());

			}

		}

		System.out.println("Cantidad de infracciones en el rango: " + arreglo.size());

		return cola;		

	}


	/**
	 * Dado	 un	 tipo	 de	 infracci�n	 (VIOLATIONCODE)	 informar	 el	 (FINEAMT) promedio	
	 *cuando	no	hubo	accidente	y	el	(FINEAMT)	promedio	cuando	si	lo	hubo.
	 * @param pViolationCode c�digo de violaci�n de la infracci�n.
	 */
	@Override
	public void darFineAMTV(String pViolationCode) {

		double fineAMTYes = 0;
		double fineAMTNo = 0;

		int contYes = 0;
		int contNo = 0;

		Iterator<VOMovingViolations> iter = stackInfracciones.iterator();

		while(iter.hasNext()) {

			VOMovingViolations actual = iter.next();

			if(actual.getViolationCode().equals(pViolationCode)) {

				if(actual.getAccidentIndicator().equals("Yes")) {

					contYes ++;
					fineAMTYes += actual.getFineAmt();

				} else {

					contNo ++;
					fineAMTNo += actual.getFineAmt();

				}

			}

		}

		System.out.println("El fineAMT de los que presentaron accidente fue: " + (fineAMTYes/contYes));
		System.out.println("El fineAMT de los que NO presentaron accidente fue: " + (fineAMTNo/contNo));

	}

	/**
	 * Consultar	las	infracciones	en	una	direcci�n	(ADDRESS_ID)	en	el rango	fecha inicial	y	
	 *fecha	 final. Ordenar	 descendentemente	 por STREETSEGID	y	 fecha
	 * @param pAdress_Id identificador de la direcci�n
	 * @param pFechaInicial fecha inicial del rango
	 * @param pFechaFinal fecha final del rango
	 * @return pila de VOMovingViolations que cumplan con los par�metros
	 */
	@Override
	public Stack<VOMovingViolations> darInfraccionesDireccion(String pAdress_Id, LocalDateTime pFechaInicial, LocalDateTime pFechaFinal) {

		Stack<VOMovingViolations> pila = new Stack<VOMovingViolations>();
		ArrayList<VOMovingViolations> arreglo = new ArrayList<VOMovingViolations>();
		VOMovingViolations aux[] = new VOMovingViolations[listaObjectId.length];

		for(int i = 0; i < listaObjectId.length; i++) {


			if((listaObjectId[i].getAdressId().equals(pAdress_Id)) && (listaObjectId[i].getTicketIssueDate().compareTo(pFechaInicial)  == 0 || listaObjectId[i].getTicketIssueDate().compareTo(pFechaInicial)  > 0)  &&  (listaObjectId[i].getTicketIssueDate().compareTo(pFechaFinal)  == 0 || listaObjectId[i].getTicketIssueDate().compareTo(pFechaFinal)  < 0)) {  
				arreglo.add(listaObjectId[i]);
				pila.push(listaObjectId[i]);

			}
		}

		Sort.ordenarMergeSort(listaObjectId, aux, 0, listaObjectId.length-1,3);
		invertirMuestra(listaObjectId);

		Scanner sc = new Scanner(System.in);

		System.out.println("�Desea ver la cantidad de infracciones resultantes?");
		System.out.println("Digite 1 para decir s�. Digite cualquier otro n�mero de lo contrario.");

		int respuesta = sc.nextInt();

		if(respuesta == 1) {

			System.out.println("�Cu�ntos datos quiere visualizar?");
			System.out.println("Digite un n�mero del 0-"+(arreglo.size()));

			int respuesta2 = sc.nextInt();

			for (int i = 0; i < respuesta2; i++) {

				VOMovingViolations actual = arreglo.get(i);

				System.out.println(actual.objectId() + "-" + actual.getTicketIssueDate() + "-" + actual.getStreetSegId() + "-" + actual.getAdressId() );

			}

		}

		System.out.println("Cantidad de infracciones en el rango: " + arreglo.size());

		return pila;

	}

	/**
	 * La	deuda	(TOTALPAID	� FINEAMT	- PENALTY1	� PENALTY2)	total por	infracciones	que	
	 *se	dieron	en	un	rango	de	fechas.
	 * @param pFechaInicial fecha inicial del rango.
	 * @param pFechaFinal fecha final del rango.
	 * @return cantidad total de pagos acerca de las infracciones en un rango dado.
	 */
	@Override
	public double darDeudaTotal(String pFechaInicial, String pFechaFinal) {

		return 0;
	}

	/**
	 * Grafica	ASCII con la	deuda	acumulada	total por	infracciones. La	grafica	es	mes	a	mes
	 *comenzando	con	el	primer	mes	del	cuatrimestre.
	 */
	@Override
	public void graficaASCII() {

		//

	}

	/**
	 * Por cada tipo mostrar su VIOLATIONCODE y el FINEAMTpromedio
	 * @return Cola   con  los  tiposde  infracciones y  su respectivo FINEAMT promedio.
	 */
	@Override
	public Queue<VOMovingViolations> getPromedioInfraccionesRango(double inf,double sup) 
	{  
		boolean loop= false;
		boolean loop1 =  false;
		int cont=0;
		double sumaAMT=0;
		double media1 = 0;
		double sumaAMTparaPromedio=0;
		int contDat=0;

		Queue<VOMovingViolations> resultado = new Queue<VOMovingViolations>();
		VOMovingViolations[] aux = listaObjectId;

		Sort.ordenarShellSort(aux, 5);
		for (int i = 0; i < aux.length; i++) {
			contDat++;
			sumaAMTparaPromedio+=aux[i].getFineAmt();
			if(aux[i].getFineAmt()==inf)
			{	
				loop = true;
			}if(aux[i].getFineAmt()<=sup)
			{
				loop1 = true;
			}else
			{
				loop1=false;
			}

			if(loop && loop1)
			{

				sumaAMT+=aux[i].getFineAmt();
				VOMovingViolations info = new VOMovingViolations(objectId, location, ticketIssueDate, totalPaid, accidentIndicator, violationDescription,String.valueOf(aux[i].getFineAmt()),aux[i].getViolationCode(),adressId,streetSegId,PENALTY1,PENALTY2);
				resultado.enqueue(info);
				cont++;
			}
		}
		media= sumaAMTparaPromedio/(double)contDat;
		media1 = sumaAMT/(double)cont;
		System.out.println("--El promedio de FINEAMT por cada infracci�n del el rango dado es: " + media1);


		return resultado;

	}
	/**
	 * Para  las  infracciones  resultantes  mostrar OBJECTID,  TICKETISSUEDAT, TOTALPAID. y se selecciona si  el  resultado  se 
		retorna descendentemente o ascendentemente por fecha de la infracci�n.
	 *@param assendente Si lo desea en orden descendente o ascendente
	 *@return pila con las infracciones.
	 **/
	@Override
	public Stack <VOMovingViolations> getInfraccionesRangoCantPagada(boolean ascendente,double limiteInf, double limiteSup) 
	{

		boolean loop= false;
		boolean loop1 =  false;
		Stack<VOMovingViolations> resultado = new Stack<VOMovingViolations>();
		VOMovingViolations[] datos = listaObjectId;
		ArrayList<VOMovingViolations> info = new ArrayList<>();
		int cont =0;
		Sort.ordenarShellSort(datos, 6);
		for(int i=0;i<datos.length;i++)
		{
			if(datos[i].getTotalPaid()==limiteInf)
			{
				loop=true;
			}
			if(datos[i].getTotalPaid()<=limiteSup)
			{
				loop1=true;
			}else
			{
				loop1=false;
			}
			if(loop==true&&loop1==true)
			{
				info.add(datos[i]);
				cont++;				
			}
		}	

		VOMovingViolations[] aux = new VOMovingViolations[info.size()];
		for (int i = 0; i < info.size(); i++)
		{
			aux[i]=info.get(i);
		}
		if(ascendente == true)
		{
			Sort.ordenarShellSort(aux, 2);

		}else if (ascendente == false)
		{
			Sort.ordenarShellSortINV(aux, 2);

		}
		for (int i = 0; i < aux.length; i++) {
			resultado.push(aux[i]);
		}
		return resultado;
	}

	/**
	 * Consultar infracciones por hora inicial y hora final, ordenada ascendentemente por VIOLATIONDESC.
	 * @param hInicial Hora inicial
	 * @param hFinal Hora final.
	 * @return  cola con las infracciones
	 */
	@Override
	public Queue <VOMovingViolations> getInfraccionesPorHora(int hInicial, int hFinal) 
	{
		Queue<VOMovingViolations> resultado = new Queue<VOMovingViolations>();
		VOMovingViolations[] datos = listaObjectId;

		Sort.ordenarShellSort(datos,7);
		ArrayList<VOMovingViolations> info = new ArrayList<VOMovingViolations>();
		boolean loop= false;
		boolean loop1 =  false;

		for(int i=0;i<datos.length;i++)
		{
			if(datos[i].darHora()==hInicial)
			{
				loop=true;
			}
			if(datos[i].darHora()<=hFinal)
			{
				loop1=true;
			}else
			{
				loop1=false;
			}
			if(loop==true&&loop1==true)
			{
				info.add(datos[i]);
			}
		}
		VOMovingViolations[] aux = new VOMovingViolations[info.size()];
		for (int i = 0; i < info.size(); i++)
		{
			aux[i]=info.get(i);
		}

		Sort.ordenarShellSort(aux, 4);
		for (int i = 0; i < aux.length; i++) {
			resultado.enqueue(aux[i]);
		}

		return resultado;
	}

	/**
	 * Busqueda binaria de un arreglo
	 * @param arreglo Arreglo en el cual se va a buscar el dato
	 * @param dato Dato a buscar
	 * @return Dato encontrado
	 */
	public static int buscar( VOMovingViolations [] arreglo, String dato) {
		int inicio = 0;
		int fin = arreglo.length - 1;
		int pos;
		Sort.ordenarShellSort(arreglo, 8);
		while (inicio <= fin) {
			pos = (inicio+fin) / 2;
			if ( arreglo[pos].getViolationCode().compareTo(dato)==0 )
				return pos;
			else if ( arreglo[pos].getViolationCode().compareTo(dato) < 0 ) {
				inicio = pos+1;
			} else {
				fin = pos-1;
			}
		}
		return -1;
	}

	public  void promedio()
	{
		double sumaAMTparaPromedio=0;
		int contDat=0;
		VOMovingViolations[] aux = listaObjectId;

		Sort.ordenarShellSort(aux, 5);
		for (int i = 0; i < aux.length; i++) {
			contDat++;
			sumaAMTparaPromedio+=aux[i].getFineAmt();
		}

		media= sumaAMTparaPromedio/(double)contDat;


	}

	/**
	 * Dado un tipo de infracci�n (VIOLATIONCODE) informar el (FINEAMT) promedio y su desviaci�n est�ndar.
	 * @param codigoViolacion codigo de violacion 
	 * @return 
	 */
	@Override
	public double[] getFineamtyDesviacion(String codigoViolacion)
	{
		if(media==0.0)
		{
			promedio();
		}

		VOMovingViolations[] lista = listaObjectId;
		double datos[] = new double[2];

		int pos = buscar(lista, codigoViolacion);
		if(pos==-1)
		{
			System.out.println("El dato no existe en este cuatrimestre intentar con otro");
		}else
		{
			double dato= lista[pos].getFineAmt();

			double prom, sum = 0; int i, n = lista.length;
			prom = media;

			for ( i = 0; i < n; i++ ) 
				sum += Math.pow ( dato - prom, 2 );

			datos[0]=media;
			datos[1]=Math.sqrt ( sum / ( double ) n );

		}
		return datos ;

	}

	/**
	 * El n�mero de infracciones que ocurrieron en un rango de horas del d�a. Se define el rango de horas por valores enteros en el rango [0, 24]
	 */
	@Override
	public int getNumeroDeInfraccionesEnIntervalo(String pHoraInicial, String pHoraFinal) 
	{

		int contadorHoras = 0;

		for(int i = 0; i < listaObjectId.length; i++) {

			String cadena = "";
			cadena = listaObjectId[i].getTicketIssueDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
			String separado[] = new String[2];
			separado = 	cadena.split("T");
			String hora = separado[1].substring(0,2);


			int horaRango = Integer.parseInt(hora);
			int horaInicial = Integer.parseInt(pHoraInicial);
			int horaFinal = Integer.parseInt(pHoraFinal);

			if(horaRango >= horaInicial && horaRango <= horaFinal) {

				contadorHoras ++;

			}


		}

		//System.out.println("Cantidad de infracciones dentro del rango" + contadorHoras);

		return  contadorHoras;
	}

	/**
	 * Grafica ASCII con el porcentaje de infracciones que tuvieron accidentes por hora del d�a.
	 */
	@Override
	public void getASCIIporcentajeAccidentesHora() 
	{

		double totalInfracciones = listaObjectId.length;
		int derecha = 0;
		int izquierda = 0;


		System.out.print("Hora");
		System.out.print("\t");
		System.out.print("|");
		System.out.print("\t");
		System.out.print("% de accidentes");
		System.out.print("\n");

		for (int i = 0; i < 22; i++) {
			
			String horaR = Integer.toString(izquierda) + Integer.toString(derecha);
			int cantidadHoras = getNumeroDeInfraccionesEnIntervalo(horaR, horaR);
			double cantidadHorasDouble = cantidadHoras;

			double porcentaje = (cantidadHorasDouble/totalInfracciones)*100;
			long porcentajeRedondeado = Math.round(porcentaje);

			if(derecha == 9) {

				System.out.print(izquierda);
				System.out.print(derecha);
				System.out.print("\t");
				System.out.print("|");
				
				for(int y = 0; y < porcentajeRedondeado; y++) {

					System.out.print("X");

				}
				
				System.out.print("\n");

				derecha = 0;
				izquierda++;

			}

			System.out.print(izquierda);
			System.out.print(derecha);
			System.out.print("\t");
			System.out.print("|");

			for(int j = 0; j < porcentajeRedondeado; j++) {

				System.out.print("X");

			}

			System.out.print("\n");

			derecha++;
		}


		System.out.print("\n");
		System.out.println("Cada X representa: 1%");

	}


	/**
	 * La deuda (TOTALPAID �FINEAMT -PENALTY1 �PENALTY2) total por infracciones que se dieron en un rango de fechas
	 * @return Deuda Total.
	 */
	@Override
	public double darDaudaTotal(LocalDateTime pFechaInicial, LocalDateTime pFechaFinal)
	{

		VOMovingViolations[] datos = listaObjectId;
		Sort.ordenarShellSort(datos,2);
		boolean loop= false;
		boolean loop1 =  false;
		int cont = 0;

		ArrayList<VOMovingViolations> lista = new ArrayList<VOMovingViolations>();
		for(int i=0;i<datos.length;i++)
		{
			double sumaTodos=0;			
			if((listaObjectId[i].getTicketIssueDate().compareTo(pFechaInicial)  == 0 || listaObjectId[i].getTicketIssueDate().compareTo(pFechaInicial)  > 0)  &&  (listaObjectId[i].getTicketIssueDate().compareTo(pFechaFinal)  == 0 || listaObjectId[i].getTicketIssueDate().compareTo(pFechaFinal)  < 0))   

			{

				cont++;
				sumaTodos+=datos[i].getFineAmt()+datos[i].getTotalPaid()+datos[i].getPenalty1()+datos[i].getPenalty2();
				lista.add(datos[i]);
			}

		}
		System.out.println("VIOLATIONDESC"+"\t \t \t \t" +"Suma Deuda Total $");
		VOMovingViolations[] aux = new VOMovingViolations[lista.size()];
		for (int i = 0; i < lista.size(); i++)
		{
			aux[i]=lista.get(i);
		}

		Sort.ordenarShellSort(datos, 4);

		for (int i = 0; i < aux.length; i++) {
			double suma=aux[i].getFineAmt()+aux[i].getTotalPaid()+aux[i].getPenalty1()+aux[i].getPenalty2();
			System.out.println(aux[i].getViolationDescription()+"\t"+"$"+suma);
		}
		return cont;
	}

	@Override
	public double deudaAcumuladaPorMesASCII()
	{
		String mes1="";
		int cont1 =0;
		String mes2="";
		int cont2 =0;
		String mes3="";
		int cont3 =0;
		String mes4="";
		int cont4 =0;
		for (int i = 0; i < primerMes/1000000; i++) 
		{	cont1++;
		mes1+="X";
		}
		
		mes2=mes1;
		cont2=cont1;
		for (int i = 0; i < segundoMes/1000000; i++) 
		{
			cont2++;
			mes2+="X";
		}
		mes3=mes2;
		cont3=cont2;
		for (int i = 0; i < tercerMes/1000000; i++) 
		{
			cont3++;
			mes3+="X";
		}
		mes4=mes3;
		cont4=cont3;
		for (int i = 0; i < cuartoMes/1000000; i++) 
		{
			cont4++;
			mes4+="X";
		}		

		System.out.println("Deuda acumulada por mes de infracciones. 2018");
		System.out.println("Mes| Dinero");
		System.out.println("01 |"+mes1+"\t \t= "+cont1 +" # de X");
		System.out.println("02 |"+mes2+"\t \t= "+cont2+" # de X");
		System.out.println("03 |"+mes3+"\t= "+cont3+" # de X");
		System.out.println("04 |"+mes4+"\t= "+cont4+" # de X");
		System.out.println("");
		System.out.println("Cada X representa $1000000 USD (Valores aproximados)");

		return 0.0;	
	}


	/**
	 * Invertir una muestra de datos (in place).
	 * datos[0] y datos[N-1] se intercambian, datos[1] y datos[N-2] se intercambian, datos[2] y datos[N-3] se intercambian, ...
	 * @param datos - conjunto de datos a invertir (inicio) y conjunto de datos invertidos (final)
	 */
	public void invertirMuestra( VOMovingViolations[ ] datos ) {
		// TODO implementar

		VOMovingViolations datosAux[] = new VOMovingViolations[datos.length];

		int pos = 0;
		for(int i = datos.length-1; i >= 0; i--) {

			datosAux[pos] = datos[i];
			pos++;
		}

		datos = datosAux;

	}



}
