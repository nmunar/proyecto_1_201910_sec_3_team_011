package controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.Scanner;

import api.IMovingViolationsManager;
import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.Stack;
import model.logic.MovingViolationsManager;
import model.vo.VOMovingViolations;
import view.MovingViolationsManagerView;

public class Controller {

	private MovingViolationsManagerView view;

	/**
	 * Cola donde se van a cargar los datos de los archivos
	 */
	private IQueue<VOMovingViolations> movingViolationsQueue;

	/**
	 * Pila donde se van a cargar los datos de los archivos
	 */
	private IStack<VOMovingViolations> movingViolationsStack;

	private static IMovingViolationsManager  manager = new MovingViolationsManager();

	private int option2 = 0;

	private boolean carga = false;


	public Controller() {
		view = new MovingViolationsManagerView();

		//TODO, inicializar la pila y la cola
		movingViolationsQueue = null;
		movingViolationsStack = null;
	}

	public void run() {
		Scanner sc = new Scanner(System.in);
		boolean fin = false;

		while(!fin)
		{
			view.printMenu();

			int option = sc.nextInt();

			switch(option)
			{
			case 1:

				if(carga == false) {

					view.printMenuCuat();
					option2 = sc.nextInt();

					carga = true;
					this.loadMovingViolations();


				} else {

					System.out.println("No se permite cargar los archivos m�s de una vez.");
					System.out.println("");

				}

				break;

			case 2:

				if(carga == true) {

					objectIdUnico();

				} else {

					System.out.println("Debe primero cargar un cuatrimestre.");

				}

				break;

			case 3:

				if(carga == true) {
					LocalDateTime fecha1;
					LocalDateTime fecha2;

					System.out.println("Digite la fecha inicial:");

					fecha1 = convertirFecha_Hora_LDT(sc.next());

					System.out.println("Digite la fecha final:");

					fecha2 = convertirFecha_Hora_LDT(sc.next());

					darRangoInfracciones(fecha1, fecha2);

				} else {

					System.out.println("Debe primero cargar un cuatrimestre.");

				}
				break;

			case 4:

				if(carga == true) {
					System.out.println("Ingrese el VIOLATIONCODE (Ej : T211)");

					String code = sc.next();

					darFineAMTV(code);
				} else {

					System.out.println("Debe primero cargar un cuatrimestre.");

				}

				break;

			case 5:

				if(carga == true) {

					view.printMessage("Ingrese el ADDRESS_ID");
					String address_Id = sc.next();

					view.printMessage("Ingrese la fecha con hora inicial (Ej : 2018-01-02T12:46:00.000Z)");
					LocalDateTime fechaInicial = convertirFecha_Hora_LDT(sc.next());

					view.printMessage("Ingrese la fecha con hora final (Ej : 2018-02-02T12:46:00.000Z)");
					LocalDateTime fechaFinal = convertirFecha_Hora_LDT(sc.next());

					darInfraccionesDireccion(address_Id, fechaInicial, fechaFinal);

				} else {

					System.out.println("Debe primero cargar un cuatrimestre.");

				}


				break;

			case 6:
				if(carga == true) {


					view.printMessage("Ingrese el limite inferior de FINEAMT  (Ej: 50)");
					double limiteInf = sc.nextDouble();

					view.printMessage("Ingrese el limite superior de FINEAMT  (Ej: 50)");
					double limiteSup = sc.nextDouble();

					IQueue<VOMovingViolations> violationCodes = violationCodesByFineAmt(limiteInf, limiteSup);
					view.printViolationCodesReq5(violationCodes);


				}else {
					System.out.println("Debe primero cargar un cuatrimestre.");
				}
				break;

			case 7:
				if(carga == true) {

					view.printMessage("Ingrese el limite inferior de TOTALPAID (Ej: 200)");
					double limiteInf = sc.nextDouble();

					view.printMessage("Ingrese el limite superior de TOTALPAID (Ej: 200)");
					double limiteSup = sc.nextDouble();

					view.printMessage("Ordenar Ascendentmente: (Ej: true)");

					boolean ascendente = sc.nextBoolean();		

					IStack<VOMovingViolations> resultados6 = getMovingViolationsByTotalPaid(ascendente,limiteInf, limiteSup);

					view.printMovingViolationReq6(resultados6);

				}else{
					System.out.println("Debe primero cargar un cuatrimestre.");
				}
				break;

			case 8:

				if(carga == true) {
					view.printMessage("Ingrese la hora inicial (Ej: 23)");
					int horaInicial7 = sc.nextInt();

					view.printMessage("Ingrese la hora final (Ej: 23)");
					int horaFinal7 = sc.nextInt();

					IQueue<VOMovingViolations> resultados7 = getMovingViolationsByHour(horaInicial7, horaFinal7);
					view.printMovingViolationsReq7(resultados7);

				} else {

					System.out.println("Debe primero cargar un cuatrimestre.");
				}

				break;

			case 9:

				if(carga == true) {
					view.printMessage("Ingrese el VIOLATIONCODE (Ej : T210)");
					String violationCode8 = sc.next();

					double [] resultado8 = avgAndStdDevFineAmtOfMovingViolation(violationCode8);

					view.printMessage("FINEAMT promedio: " + resultado8[0] + ", desviaci�n estandar:" + resultado8[1]);
				} else {

					System.out.println("Debe primero cargar un cuatrimestre.");
				}

				break;

			case 10: 

				if(carga == true) {

					view.printMessage("Ingrese la hora inicial (Ej: 23)");

					String horaInicial = sc.next();

					view.printMessage("Ingrese la hora final (Ej: 23)");

					String horaFinal = sc.next();

					getNumeroInfraccionesEnIntervalo(horaInicial, horaFinal);

				} else {

					System.out.println("Debe primero cargar un cuatrimestre.");
				}
				break;
	
			case 11:
				
				if(carga == true) {
					
					getASCIIporcentajeAccidentesHora();
					
				} else {
					
					System.out.println("Debe primero cargar un cuatrimestre.");
					
				}
				
				break;

			case 12:
				if(carga == true) {

					view.printMessage("Ingrese la fecha con hora inicial (Ej : 2018-01-09T11:18:00.000Z)");
					LocalDateTime fechaInicial = convertirFecha_Hora_LDT(sc.next());

					view.printMessage("Ingrese la fecha con hora final (Ej : 2018-01-10T11:18:00.000Z)");
					LocalDateTime fechaFinal = convertirFecha_Hora_LDT(sc.next());

					getDeudaTotal(fechaInicial, fechaFinal);
				}
				break;
			case 13:
				if(carga == true) {

					getACIIdeudaPorMes();
				}
				break;

			case 14:	
				fin=true;
				sc.close();
				break;

			}
		}
	}



	private double getACIIdeudaPorMes() {
		return manager.deudaAcumuladaPorMesASCII();

	}

	private double[] avgAndStdDevFineAmtOfMovingViolation(String violationCode) 
	{
		return manager.getFineamtyDesviacion(violationCode);
	}

	private IQueue<VOMovingViolations> getMovingViolationsByHour(int hInicial, int hFinal) {
		return manager.getInfraccionesPorHora(hInicial, hFinal);
	}

	public  void loadMovingViolations() {
		// TODO
		manager.loadMovingViolations(option2, 1);
	}

	public void objectIdUnico() {

		manager.verifyObjectIDIsUnique();

	}

	public void darRangoInfracciones(LocalDateTime pFechaInicial, LocalDateTime pFechaFinal) {

		manager.darRangoInfracciones(pFechaInicial, pFechaFinal);	

	}

	public void darFineAMTV(String pViolationCode) {

		manager.darFineAMTV(pViolationCode);

	}

	public void darInfraccionesDireccion(String pAdress_Id, LocalDateTime pFechaInicial, LocalDateTime pFechaFinal) {

		manager.darInfraccionesDireccion(pAdress_Id, pFechaInicial, pFechaFinal);

	}
	public IQueue<VOMovingViolations> violationCodesByFineAmt(double limiteInf, double limiteSup) {

		return manager.getPromedioInfraccionesRango(limiteInf, limiteSup);
	}

	public IStack<VOMovingViolations> getMovingViolationsByTotalPaid(boolean assendente, double limiteInf, double limiteSup)
	{
		return manager.getInfraccionesRangoCantPagada(assendente,limiteInf,limiteSup);
	}

	private static LocalDateTime convertirFecha_Hora_LDT(String fechaHora)
	{
		return LocalDateTime.parse(fechaHora, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss'.000Z'"));
	}

	public double getDeudaTotal(LocalDateTime pFechaInicial, LocalDateTime pFechaFinal)
	{
		return manager.darDaudaTotal(pFechaInicial, pFechaFinal);
	}

	public int getNumeroInfraccionesEnIntervalo(String pHoraInicial, String pHoraFinal) {

		return manager.getNumeroDeInfraccionesEnIntervalo(pHoraInicial, pHoraFinal);

	}
	
	public void getASCIIporcentajeAccidentesHora() {
		
		manager.getASCIIporcentajeAccidentesHora();
		
	}

}
