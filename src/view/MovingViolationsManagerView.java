package view;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.vo.VOMovingViolations;

public class MovingViolationsManagerView 
{
	public MovingViolationsManagerView() 
	{

	}

	public void printMenu() {

		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cargar datos del cuatrimestre");
		System.out.println("2. Verificar que OBJECTID es en realidad un identificador único");
		System.out.println("3. Consultar infracciones por fecha/hora inicial y fecha/hora final");
		System.out.println("4. Dar FINEAMT promedio con y sin accidente por VIOLATIONCODE");
		System.out.println("5. Consultar infracciones por direccion entre fecha inicial y fecha final");


		System.out.println("6. Consultar los tipos de infracciones (VIOLATIONCODE) con su valor (FINEAMT) promedio en un rango dado");
		System.out.println("7. Consultar infracciones donde la cantidad pagada (TOTALPAID) esta en un rango dado. Se ordena por fecha de infraccion");
		System.out.println("8. Consultar infracciones por hora inicial y hora final, ordenada ascendentemente por VIOLATIONDESC");
		System.out.println("9. Dado un tipo de infracción (VIOLATIONCODE) informar el (FINEAMT) promedio y su desviación estándar.");

		System.out.println("10. El número de infracciones que ocurrieron en un rango de horas del día. Se define el rango de horas por valores enteros en el rango [0, 24]");
		System.out.println("11. Grafica ASCII con el porcentaje de infracciones que tuvieron accidentes por hora del día");
		System.out.println("12. La deuda (TOTALPAID – FINEAMT - PENALTY1 – PENALTY2) total por infracciones que se dieron en un rango de fechas.");
		System.out.println("13. Grafica ASCII con la deuda acumulada total por infracciones");


		System.out.println("14. Salir");
		System.out.println("Digite el número de opción para ejecutar la tarea, luego presione enter: (Ej., 1):");

	}

	public void printMenuCuat() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Proyecto_1----------------------");
		System.out.println("1. Enero, Febrero, Marzo, Abril");
		System.out.println("2. Mayo, Junio, Julio, Agosto");
		System.out.println("3. Septiembre, Octubre, Noviembre, Diciembre");
		System.out.println("Digite el número de opción para elegir el cuatrimestre del año que desea cargar, luego presione enter: (Ej., 1):");

	}

	public void printMovingViolations(IStack<VOMovingViolations> violations) {
		System.out.println("Se encontraron "+ violations.size() + " elementos");
		for (VOMovingViolations violation : violations) 
		{
			System.out.println(violation.objectId() + " " 
					+ violation.getTicketIssueDate() + " " 
					+ violation.getLocation()+ " " 
					+ violation.getViolationDescription());
		}
	}

	public void printViolationCodesReq5(IQueue<VOMovingViolations> violationCodes) {
		System.out.println("VIOLATIONCODE"+"\t"+ "FINEAMT promedio");

		for(VOMovingViolations v: violationCodes) {
			System.out.println(v.getViolationCode() + "\t" + v.getFineAmt());
		}
	}

	public void printMovingViolationReq6(IStack<VOMovingViolations> resultados6) {
		System.out.println("OBJECTID"+"\t"+"TICKETISSUEDAT"+"\t"+"TOTALPAID");
		for(VOMovingViolations v: resultados6) {
			System.out.println( v.objectId() + "\t" + v.getTicketIssueDate() + "\t" + v.getTotalPaid());
		}
	}

	public void printMovingViolationsReq7(IQueue<VOMovingViolations> resultados7) {
		System.out.println("OBJECTID"+"\t" +"TICKETISSUEDAT"+"\t"+"VIOLATIONDESC");
		for(VOMovingViolations v: resultados7) {
			System.out.println( v.objectId() + "\t" + v.getTicketIssueDate() + "\t" + v.getViolationDescription());
		}
	}
	public void printMessage(String mensaje) {
		System.out.println(mensaje);
	}


}
