package api;

import java.time.LocalDateTime;

import model.data_structures.Queue;
import model.data_structures.Stack;
import model.vo.VOMovingViolations;

/**
 * Basic API for testing the functionality of the STS manager
 */
public interface IMovingViolationsManager {

	/**
	 * Method to load the Moving Violations of the STS
	 * @param movingViolationsFile - path to the file 
	 */
	void loadMovingViolations(int pRespuestaCuat, int pMetodo);


	public Queue<VOMovingViolations> getInfraccionesPorHora(int hInicial, int hFinal);

	double[] getFineamtyDesviacion(String codigoViolacion);

	int getNumeroDeInfraccionesEnIntervalo(String pHoraInicial, String pHoraFinal);

	void getASCIIporcentajeAccidentesHora();

	void verifyObjectIDIsUnique();

	Queue<VOMovingViolations> darRangoInfracciones(LocalDateTime pFechaInicial, LocalDateTime pFechaFinal);

	void darFineAMTV(String pViolationCode);

	Stack<VOMovingViolations> darInfraccionesDireccion(String pAdress_Id, LocalDateTime pFechaInicial, LocalDateTime pFechaFinal);

	double darDeudaTotal(String pFechaInicial, String pFechaFinal);

	void graficaASCII();

	Queue<VOMovingViolations> getPromedioInfraccionesRango(double a, double b);

	Stack<VOMovingViolations> getInfraccionesRangoCantPagada(boolean assendente,double limiteInf, double limiteSup);


	double darDaudaTotal(LocalDateTime pFechaInicial, LocalDateTime pFechaFinal);


	double deudaAcumuladaPorMesASCII();


}
